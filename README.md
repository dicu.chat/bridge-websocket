<h1 align="center">
  <br>
  <a href="https://www.morphysm.com/"><img src="./assets/morph_logo_rgb.png" alt="Morphysm" ></a>
  <br>
  <h5 align="center"> Morphysm is a community of engineers, designers and researchers
contributing to security, cryptography, cryptocurrency and AI.</h5>
  <br>
</h1>

<h1 align="center">
 <img src="https://img.shields.io/badge/Python-3.9-brightgreen" alt="python badge">
 <img src="https://img.shields.io/badge/docker-20.10-blue" alt="docker badge">
 <img src="https://img.shields.io/badge/version-1.1.8-orange" alt="version badge">
 <img src="https://img.shields.io/gitlab/pipeline-status/dicu.chat/server?branch=master" alt="docker build">
</h1>

# Table of Contents

<!--ts-->

- [Getting Started](#getting-started)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Development](#development)
- [Production](#production)
- [TroubelShoting](#troubelShoting)
- [List of Changes](#list-of-changes)
- [Code Owners](#code-owners)
- [License](#license)
- [Contact](#contact)
<!--te-->

# 1. Getting Started

This repository connects our chatbot server to our web widget application, by constructing message bridges from the botSociety conversation flow to the server code and sending it to the widget chat. Please keep in mind that in order to understand the functionality, you must also run our [server repo](https://gitlab.com/dicu.chat/server) at the same time with this repo to see the desired results, otherwise, it will work but with no obvious results.

# 2. Prerequisites

Please make sure that your system has the following platforms:

- `docker`
- `docker-compose`
- `python3.9`

To download docker-compose & docker you can run these commands. For more info you can click [here!](https://docs.docker.com/desktop/):

```bash
 sudo snap install docker
 sudo apt  install docker-compose
```

Then connect your user with the docker by running this command:

```bash
 sudo usermod -aG docker $USER
```

To make sure everything that the docker has been installed please run:

```bash
 docker ps
```

# 3. Installation

To download our repo please wite this command either with SHH:

```bash
 git clone git@gitlab.com:dicu.chat/bridge-websocket.git
```

or with HTTPS:

```bash
 git clone https://gitlab.com/dicu.chat/bridge-websocket.git
```

# 4. Development

Please follow these steps to help you run our project (Not Dokerized):

1. Create dev python environment for the project:

   ```bash
   virtualenv -p python3.9 .venv && source .venv/bin/activate
   ```

2. Install dependencies:

   ```bash
   python -m pip install -r requirements.txt
   ```

3. Copy the `.env` file:

   ```bash
   cp .env.example .env
   ```

4. Fille the `.env` file with the SERVER_TOKEN: You can generate a random string and fill the server token in the env file, but keep in mind that this token must also be filled in the server repo please check it out [here!](https://gitlab.com/dicu.chat/server/-/blob/master/.env.example#L3)

   Example:

   ```js
   SERVER_TOKEN = token;
   ```

5. Before run the server of this project please dont forget to run the our [main server](https://gitlab.com/dicu.chat/server) then back here and Run the project:

   ```bash
   python -m server
   ```

   our server will be look like this after running it:
   ![server](/assets/server-brdige.png)

6. To see real-world results from our web widget chat bot, visit our react widget repo [here!](https://gitlab.com/dicu.chat/react-chat-widget) and then follow the on-screen instructions to run it locally.

# 5.Production

For examples on how to set this bridge up with server core [click here!](https://gitlab.com/dicu.chat/server/-/tree/master/examples/all).

# 6. TroubelShoting

If you have encountered any problems while running the code, please open a new issue in this repo and label it bug, and we will assist you in resolving it. :wink:

# 7. Code Owners

@morphysm/team :sunglasses:

# 8. License

Our reposiorty is licensed under the terms of the GPLv3 see the license [here!](https://gitlab.com/dicu.chat/bridge-websocket/-/blob/master/LICENSE)

# 9. Contact

If you'd like to know more about us [Click here!](https://www.morphysm.com/), or You can contact us at "contact@morphysm.com".
