import logging
import uuid


def init(app, sio, config: dict, cache: dict):

    @sio.event
    async def connect(sid, _environ):
        logging.info("Connected: %s", sid)

    @sio.event
    async def disconnect(sid):
        logging.info("Disconnected: %s", sid)

    @sio.event
    async def start(sid, event):
        # Store/Create session to track this user's history.
        if event["session"] is None:
            session = str(uuid.uuid4())
            # Send new session back to the user.
            await sio.emit("update_session", {"session": session}, room=sid)
        else:
            session = event["session"]

        # Create 'update_commands' event:
        await app.dispatch(
            "api.export.commands",
            context={"session": session}
        )

        # New/Old user handling.
        if session not in cache:
            cache[session] = {
                "history": list(),
                "sid": sid,
            }

            await app.dispatch(
                "api.message.incoming",
                context={"session": session, "initial": True}
            )

        else:
            cache[session]["sid"] = sid

            # Load user's history
            for message in cache[session]["history"]:
                # send data via corresponding websocket to the user
                await sio.emit("new_message", message, room=sid)

    @sio.event
    async def new_message(sid, event):
        await app.dispatch(
            "api.message.incoming",
            context={"session": event["session"], "text": event["text"]}
        )

    @sio.event
    async def new_voice(sid, event):
        await app.dispatch(
            "api.message.incoming",
            context={
                "session": event["session"], "voice": event["voice"],
                "extension": event.get("extension"), "codec": event.get("codec"),
            }
        )
