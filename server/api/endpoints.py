from sanic.response import json
import logging
import time


def init(app, sio, config: dict, cache: dict):

    @app.route("/", methods=['POST'])
    async def webhook_from_server(request):
        # Authorize server requests:
        if request.token != config['server_token']:
            logging.warning("Unauthorized attempt to make a post request with payload: %s, headers: %s", data, request.headers)
            return json({"status": 400, "message": "Unauthorized attempt to make a post request!", "timestamp": time.monotonic()}, status=400)

        data = request.json

        if not isinstance(data, dict):
            return json({"status": 400, "message": f"Malformed body! Expected 'object', got '{type(data)}'."}, status=400)

        session = data['chat']['chatId']

        # save message to the history
        data["isBot"] = True
        q = cache[session]['history']
        q.append(data)
        # if history is too long -> pop oldest message
        if len(q) > config["maxsize"]:
            q.pop(0)

        try:
            # send data via corresponding websocket to the user
            await sio.emit("new_message", data, room=cache[session]["sid"])
            # respond to the server according to info/debug style schema
            return json({"status": 200, "timestamp": time.monotonic()})
        except Exception as e:
            logging.warning("Exception during propagating message from server '%s'. Error: '%s'", session, str(e))
            return json({"status": 500, "message": str(e), "timestamp": time.monotonic()})

    @app.route("/api/export/commands", methods=["POST"])
    async def export_commands_server(request):
        if request.token != config["server_token"]:
            logging.warning("Unauthorized attempt to make a post request with commands, payload: %s", request.json)
            return

        commands = request.json

        # Propagate commands event to all users.
        for session, cached in cache.items():
            await request.app.dispatch(
                "api.export.commands",
                context={"session": session, "data": commands}
            )

        logging.info("Triggered commands update event for %s users..", len(cache))
        # respond to the server according to info/debug style schema
        return json({"status": 200, "timestamp": time.monotonic()})
