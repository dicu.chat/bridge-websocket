from copy import deepcopy
import aiohttp
import logging


def init(app, sio, config: dict, cache: dict):

    @app.signal("api.message.incoming")
    async def propagate_message_incoming(**context):
        session = context["session"]

        # Event must have text or bool 'initial' set.
        if context.get("initial"):
            context["text"] = "/start"

        headers = {
            "Authorization": f"Bearer {config['instance_token']}",
            "Instance-Name": config["instance_name"],
            "Service-In": "bridge-websocket",
        }

        text = context.get("text")
        if voice := context.get("voice"):
            voice = {
                "content": voice,
                "mediaType": "voice",
                "contentType": "bytes",
                "extension": context.get("extension"),
                "codec": context.get("codec"),
            }
            # Setting text to None, because there must be only one of them filled.
            text = None

        # Prepare payload for the message
        payload = {
            "user": {
                "firstName": f"WebUser({session})",
                "userId": session
            },
            "chat": {
                "chatId": session
            },
            "message": {
                "text": text,
                "voice": voice,
            },
            "context": {
                "wanted_extension": context.get("extension"),
                "wanted_codec": context.get("codec"),
            },
            "files": [],
            "buttons": [],
        }

        if not context.get("initial"):
            payload["isBot"] = False
            cache[session]["history"].append(deepcopy(payload))
            del payload["isBot"]

        async with aiohttp.ClientSession(headers=headers) as client:
            async with client.post(f"{config['server_url']}/api/process_message", json=payload) as r:
                logging.info("Sent message to the server, status '%s': %s", r.status, await r.json())
                if r.status != 200:
                    logging.exception("Error in the initial response from the server: %s", await r.json())

    @app.signal("api.export.commands")
    async def propagate_commands_event(**context):
        if "data" not in context:
            headers = {
                "Authorization": f"Bearer {config['instance_token']}",
                "Instance-Name": config["instance_name"],
            }
            async with aiohttp.ClientSession(headers=headers) as client:
                async with client.get(f"{config['server_url']}/api/export/commands") as r:
                    if r.status != 200:
                        logging.exception("Error in the commands request: %s", await r.text())
                        return

                    context["data"] = await r.json()

        try:
            # context["data"] is expected to be a dict with commands.
            await sio.emit("update_commands", context["data"], room=cache[context["session"]]["sid"])
        except Exception as e:
            logging.warning("Exception during propagating commands event for user '%s'. Error: '%s'", context["session"], str(e))
