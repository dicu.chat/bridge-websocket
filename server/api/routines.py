import aiohttp


def init(app, sio, config: dict, cache: dict):

    @app.listener("before_server_start")
    async def setup(_app, _loop):

        headers = {
            "Authorization": f"Bearer {config['server_token']}",
        }

        data = {
            "url": config["webhook"],
        }

        async with aiohttp.ClientSession() as session:
            async with session.post(f"{config['server_url']}/api/setup", json=data, headers=headers) as r:
                result = await r.json()
                if result['status'] == 200:
                    config["instance_token"] = result['token']
                    config["instance_name"] = result['name']
