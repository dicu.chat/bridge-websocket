from sanic_cors import CORS, cross_origin
from sanic import Sanic
import socketio
import logging
import dotenv
import os

from .api import routines, sockets, signals, endpoints


# load bot token from .env
env_path = '.env'
dotenv.load_dotenv(env_path)
# Configuration
CONFIG = {
    # Security tokens:
    "server_token": os.getenv("SERVER_TOKEN"),
    # Sever url:
    "server_url": os.getenv("SERVER_URL"),
    # Default port:
    "port": int(os.getenv("PORT", 8080)),
    # Webhook:
    "webhook": os.getenv("WEBHOOK"),
    # Instance data:
    "instance_token": None,
    "instance_name": None,
    # Max history size:
    # TODO: use REDIS (or something) to make instance's cache "reload-consistent"
    "maxsize": 15,
}
CACHE = dict()

sio = socketio.AsyncServer(async_mode="sanic", cors_allowed_origins=[])
app = Sanic("dicu-bridge-websocket")
app.config['CORS_SUPPORTS_CREDENTIALS'] = True
CORS(app)
sio.attach(app)

endpoints.init(app, sio, CONFIG, CACHE)
routines.init(app, sio, CONFIG, CACHE)
sockets.init(app, sio, CONFIG, CACHE)
signals.init(app, sio, CONFIG, CACHE)


if __name__ == "__main__":
    # Logging
    formatter = "%(asctime)s - %(filename)s - %(levelname)s - %(message)s"
    date_format = "%d-%b-%y %H:%M:%S"
    logging.basicConfig(format=formatter, datefmt=date_format, level=logging.INFO)

    app.run(host="0.0.0.0", port=CONFIG["port"])
